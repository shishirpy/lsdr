"""
CPLST module for the paper.
Usage with one dataset is shown.
"""


import numpy as np
from numpy.linalg import svd
from sklearn.linear_model import Ridge
from scipy import io as sio
from sklearn.metrics import hamming_loss as hl
import time



data_path = "data/"

class CPLST():        
    def encode(self, X_tr, Y_tr, k):
        """
        Conditional Pricipal Label Space Transformation
        """
        start_time = time.time()
        y_bar = np.average(Y_tr, axis = 0) #mean of output
        Z = Y_tr - y_bar #shift mean to zero.
        
        H = np.dot(X_tr, np.linalg.pinv(X_tr)) #hat matrix
        
        M = np.dot(Z.T, np.dot(H, Z))
        
        A, Sigma, B = svd(M)
        
        V_k = B[:k, :] #pick top k eigen vectors
        
        T = np.dot(V_k, Z.T)
        T = T.T
        
        rg = Ridge(alpha = 0.1)
        rg.fit(X_tr, T)
        
        print time.time() - start_time
        
        return rg, V_k, y_bar
        
    def decode(self, X, rg, V_k, y_bar):
        T = rg.predict(X)
        Y = np.sign(np.dot(T, V_k) + y_bar)
        return Y
        
        
        
if __name__ == "__main__":
    dataset = 'emotions' 
    X = sio.mmread(data_path + 'emotions/X_tr.mtx')
    Y = sio.mmread(data_path + 'emotions/Y_tr.mtx')    
    X = X.todense()
    Y = Y.todense()
    Y[Y==0] = -1
    
    X_tt = sio.mmread(data_path + 'emotions/X_tt.mtx')
    Y_tt = sio.mmread(data_path + 'emotions/Y_tt.mtx')
    
    X_tt = X_tt.todense()
    Y_tt = Y_tt.todense()
    Y_tt[Y_tt==0] = -1
    
    
    cplst = CPLST()
        
    for i in [2, 3, 4]: #[75, 150, 225]:
        rg, V_k, y_bar = cplst.encode(X, Y, i)
        Y_hat = cplst.decode(X_tt, rg, V_k, y_bar)
        print "hl = ", hl(Y_hat, Y_tt)
    
    