"""
contains the method present in the paper ML-CSSP
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge
import time


def EMCML(X_tr, Y_tr, X_t, Y_t, k ):
    """
    Parameters
    --------------
    X_tr - Train input
    
    Y_tr -- Train output
    
    X_t - Test input
    
    Y_t - Test Ouput
    
    k - Number of lanmark columns
    
    
    
    Return
    ----------
    
    A  - predicted output (full label space)
    """
    #print "Entered EMCML, k = ", k
#    start_time = time.time()
    
    cols = CSS(Y_tr, k)  #get the landmark sorted columns
    cols = sorted(cols)
    
    ridge = Ridge(alpha = 0.1)
    
    
    Ytr = Y_tr[:, cols]
    Xtr = X_tr
    
    ridge.fit(Xtr, Ytr)
    h = ridge.predict(X_t)
#    Ytt = Y_t[:, cols]
#    print h.shape, Ytr.shape, Ytt.shape
#    print h.T.shape, np.linalg.pinv(Ytr).shape, Y_tr.shape
    
    A = np.dot(np.dot(h, np.linalg.pinv(Ytr)), Y_tr)
    #print A
    
    #print "done with EMCML"
    #print time.time() - start_time, "sec"
    return A
    
    
    
    
    
def CSS(Y, k):
    """
    Column Subset selection of matrix k.

    Parameters
    ------------
    Y - is the ouput matrix whose singular values we need 
    to calculate
    
    k - Number of lanmark variables to use.
    """
    u, s, v = np.linalg.svd(Y)
    
    V = v[:, :k]
    
    p = list()  #probability list for various columns
    #C = list()
    
    for row in V:
        p.append((np.linalg.norm(row)**2)/k)
        
    P = [0]
    
    su = 0
    for i in p:
        su += i
        P.append(su)
        
    C =  SamplefromCDF(P, k)
    #return C
    return sorted(C)
        
        
def SamplefromCDF(P, n, replacement = False):
    """
    Discrete samples from a given CDF.
    
    Parameters
    -----------
    
    P - the CDF of the given distributions
    
    n - number of samples to be picked   
    
    replacement - (bool, default = False)  #not added yet
    weather to pick the samples with replacement or without
    
    """
    
    m = len(P)
    
    if P[0] != 0:
        print "first element of P must be 0"
        return 0
        
    samples = list()  #with contain the samples

    done = False
    
    
    while not done:
        t = np.random.rand()
        for j in range(m):
            if j != m-1:
                if t > P[j] and t < P[j+1]:
                    if j in samples:
                        break 
                    else:
                        samples.append(j)
                        #print len(samples)
                        if len(samples) == n:
                            done = True
                            break
            """
            else:
                if j not in samples:
                    #print len(samples)
                    #samples.append(j)
                    if len(samples) == n:
                        done = True
            """
    return samples
    
    
if __name__ == "__main__":
    
    np.random.seed(0)
    m, n = 40, 500
    k = 300
    Y = np.random.randn(m, n)
    
    
    np.random.seed()
    u, s, v = np.linalg.svd(Y)
    
    v1 = v[:, :k]
    
    print v1.shape
    
    p = list()
    
    for row in v1:
        p.append((np.linalg.norm(row)**2)/k)
        
    P = [0]
    
    su = 0
    for i in p:
        su += i
        P.append(su)
        
    samples = SamplefromCDF(P, k)
    
    #print p
    print 
    print samples
    print 
    #print P
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_ylim(0,1)
    ax.plot(p)
    ax.plot(P)
    plt.show()