import numpy as np
from sklearn.linear_model import LinearRegression as LR
#from sklearn.linear_model import Ridge
#import time
#import pdb
import time

def GreedySelect2(M, k):
    """
    MI = Mutual Information.
    Greedily select which rows maximize MI.

    Input
    ------
    M : Matrix whose rows needs to be selected (M must be a square matrix)

    k : Number of rows to be selected



    Returns
    -------
    idxs : Index of the rows that contribute to maximizing of the MI
    
    T : respective rows of the channel matrix
    """

    #print time.ctime()

    M = np.matrix(M)
    m,n = M.shape

    """
    if m != n:
        print "Matrix M must be square"
        return -1, -1
    """

    
    T = []  #rows finally selected by the algorithm
    idxs = []
    R = np.zeros((1,n))
    T = R
    #print T.shape, R.shape, M.shape
    #pdb.set_trace()
    
    for i in xrange(k):
        #print time.ctime()
        val = 0
        tmp = 0
        idx = 0
        #pdb.set_trace()
        #print i
        
        for j in xrange(m):
            if j not in idxs:
                #print T.shape,R.shape, M.shape
                R = np.concatenate((T,M[j,:]))
            else:
                continue

            if i == 0:
                R = R[1,:]
            #print T.shape, R.shape, M.shape
            #start_time = time.time()
            val = np.linalg.det(np.eye(R.shape[0])+ R*R.T)
            #print "GS2 = ", time.time() - start_time
            #print val,
            if val > tmp:
                idx = j
                tmp = val

        T = np.concatenate((T, M[idx,:]))
        #print idx
        #print
        if i == 0:
            T = T[1,:]
        idxs.append(idx)

    
    return idxs, T.T