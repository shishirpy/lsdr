"""
This file contains procedures to reproduce the results on multi-label datasets.
This program only produces the results for ILS, PLST, ML-CSSP.

You will need a working installation of python and all required libraries to 
run it. We recommend you download "Anaconda Python" from 
http://continuum.io/downloads

It has all the required libraries.
"""



import numpy as np
#import scipy as sp
from sklearn.linear_model import Ridge
from sklearn.cross_validation import KFold
from greedyselect import GreedySelect2 as gs2
#import util
from sklearn.linear_model import Lasso
import matplotlib.pyplot as plt
from sklearn.metrics import hamming_loss as hl
#from sklearn.linear_model import LinearRegression as lr
import scipy.io as sio
from sklearn.cross_validation import train_test_split as tts
import EMCML
import time


data_path = "data/"
train_X = "X_tr"
train_Y = "Y_tr"

test_X = "X_tt"
test_Y = "Y_tt"

RidgeAlphas = {'bibtex':0.1, 'yeast': 0.1, 'corel5k': 0.1, \
'delicious':0.1, 'emotions':0.1, 'scene': 0.1, 'enron': 0.1}


def GenDataPath(dataset):
    """
    Note that the names of the test and train files will 
    remain the same. All we need to do it to change data_path
    """
    global data_path
    data_path = data_path + dataset + "/"
    

def read_train_data(sparse = False):
    """
    Read Data from the training files
    
    Parameters
    ----------------
    sparse - Weather the input data to be read is in matrix market format
    
    Return
    ----------------
    X - input variables (with each row represnting different data pt.)
    
    Y - corresponding output(points arranged rowwise)
    """
    if sparse:
        X = sio.mmread(data_path + train_X + '.mtx')
        X = X.todense()
        X = np.array(X)
        #Y = np.loadtxt(data_path + train_Y, dtype = 'float32')
        Y = sio.mmread(data_path + train_Y + '.mtx')
        Y = Y.todense()
        Y = np.array(Y)
        Y[Y==0] = -1  #convert binary output with -1, 1 classes
        return X, Y
    else:
        X = np.loadtxt(data_path + train_X, dtype = 'float32')
        Y = np.loadtxt(data_path + train_Y, dtype = 'float32')   
        return X, Y



def read_test_data(sparse=False):
    """
    Read Test Data
    Check the help for read_train_data
    """
    if sparse:
        X = sio.mmread(data_path + test_X + '.mtx')
        X = X.todense()
        X = np.array(X)
        
        Y = sio.mmread(data_path + test_Y + '.mtx')
        Y = Y.todense()
        Y = np.array(Y)
        Y[Y==0] = -1    
        return X, Y
        
    else:        
        X = np.loadtxt(data_path + test_X, dtype = 'float32')
        Y = np.loadtxt(data_path + test_Y, dtype = 'float32')    
    return X, Y
    
    
def FindLMIndex(X, Y, alpha = 0.5, k = 5):
    """
    This function will do only the Ridge regression part. And 
    find the landmark variables.
    
    Parameters
    ------------
    X - Input variables
    
    Y - Corresponding output 
    
    alpha - alpha value to be used for Ridge regression (default = 0.5)
    
    k - Guess of Landmark variables (default = 5) 
    
    
    Returns
    -------------
    idx - sorted index of the landmark columns
    
    intercepts - returns the intercept of the regression line
    
    T - 
    """
    
    clf = Ridge(alpha=alpha)  #alpha from cross-validation
    Z1 = clf.fit(X,Y).coef_
    m, n = X.shape  #number of points
    

    idx , T = gs2(Z1,k)   #k - guess of the landmark variables
    

    d = [idx.index(i) for i in sorted(idx)]  #to order the cols of T
    
    idx = sorted(idx)
    intercepts = list()
    
    for i in idx:
        intercepts.append(clf.intercept_[i])
        
    
    return idx, intercepts, T[:, d]
    
    
def TrainLM(idx, Y):
    """
    Training to go from the landmark variables to the complete
    Matrix.
    
    i.e. to go from the compressed label space to the entire space
    
    Parameters
    -------------
    idx - sorted index of the landmark columns
    
    Y - Complete output matrix on which to train.
    
    Return
    --------------
    LR - Ridge regressor (alpha = 0.1)
    """
    idx = sorted(idx)
    
    #LR = lr()   #linear regression ordinary
    LR = Ridge(alpha = 0.1)
    
    m, n = Y.shape
    Y1 = Y[:, idx]  #split the test and train
    LR.fit(Y1, Y)
    
    return LR


def PLST(X, Y, X_t,  Y_t, k = 5, train = True):
    """
    Implementation of the PLST Algorithm
    
    Parameters
    ----------------------
    
    X = train input sequence
    
    Y = train output
    
    X_t = test input
    
    Y_t = test output
    
    k = number of principle components
    
    train = if train or test data
    """
    if train:
        #start_time = time.time()
        U, S, V = np.linalg.svd(Y, full_matrices = False)
        s_dia = np.diag(S[:k])
        V = V.T
        V_prime = V[:, :k]
        S_prime = s_dia
        m, n = S_prime.shape
        t = U.shape[1]
        
        S_prime = np.vstack((S_prime, np.zeros((t-m, n))))
        
        Y_tr = np.dot(U,S_prime)
               
        #ridge = Ridge(alpha = 0.01)
        ridge = Ridge(alpha = 0.1 )
        ridge.fit(X,Y_tr)
        y_hat = ridge.predict(X_t)
        
        Y_T = np.dot(y_hat, V_prime.T)
        Y_T = np.sign(Y_T)
        
        hammingLoss = hl(Y_t, Y_T)
        
        #print "Total time in PLST, k = ", k , "is = ", time.time()-start_time
        return hammingLoss
        
        
    else:
        pass

    
    
def Input2Output(dataset, k, sparse = False):
    """
    This method will map the input to the ouput.
    But also does the cross validation step.
    """
    
    
    print "THIS METHOD IS NOT CORRECT. AVERAGING IS NOT DONE \
    PROPERLY"
    GenDataPath(dataset)
    X, Y = read_train_data(sparse)
    X_t, Y_t  = read_test_data(sparse)
    

    """
    The following part will be for averaging.
    """
    #make big dataset for cross-validation
    X_1 = np.concatenate((X, X_t))
    Y_1 = np.concatenate((Y,Y_t))
    
    
    
    #alpha below needs to be stored somewhere 
    alpha = RidgeAlphas[dataset]
    #alpha = 0.1
    ks = list(k)
    
    error_mean = list()    
    error_std = list()
    for i in ks:    
        print i
        error_i = []
        for count in xrange(20):
            X, X_t, Y, Y_t = tts(X_1,Y_1, test_size = 0.1)
            idx, intercepts, beta = FindLMIndex(X, Y, alpha, i)
            LR = TrainLM(idx,Y)  #Linear Regression on the output
            
            Y1 = np.dot(X_t, beta)
            Y1 += intercepts
            
            y_hat = LR.predict(Y1)
            hammingloss = hl(np.sign(y_hat), Y_t)
            hloss = PLST(X,Y, X_t, Y_t, k = i)
            #EMCML stuff
            y_hat_EMCML = EMCML.EMCML(X, Y, X_t, Y_t, k = i)
            EMCML_loss = hl(np.sign(y_hat_EMCML), Y_t)
            
            #append all errors
            error_i.append([hammingloss, hloss, EMCML_loss])
        error_i = np.array(error_i)
        avg = np.average(error_i, axis = 0)
        std = np.std(error_i, axis = 0)
        #error vector is = [m, IVS, PLST, ML-CSSP]
        error_mean.append([i, avg[0], avg[1], avg[2]])
        error_std.append([i, std[0], std[1], std[2]])
        
    #WriteError2File(dataset, error)
        
    np.savetxt(data_path + "mean_error_1_1.csv", np.array(error_mean), delimiter = ',')
    np.savetxt(data_path + "std_error_1_1.csv", np.array(error_std), delimiter = ',')
    
    global data_path 
    data_path = "data/"
    
    
    
    PlotData(error_mean, "Input2Output:" + dataset)
    return error_mean
    
    
    
    
def PlotData(error, dataset):
    error = np.asarray(error)
    m, n = error.shape
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('Number of Landmark Variables')
    ax.set_ylabel('Hamming Loss')
    ax.set_title(dataset)
    ax.grid('on')
   
    x = error[:, 0]
    
    labels = ['IVS', 'PLST', 'ML-CSSP']
    for i in xrange(1,n):
        ax.plot(x, error[:, i], label = labels[i-1])
        
    
    ax.legend(loc='center', shadow=True)
        
    plt.show()
    np.set_printoptions(precision = 3)
    print repr(error)
    
    

    
if __name__ == "__main__":
    ks = [75, 150, 225]
    #ks = [19]
    Input2Output('corel5k', ks, sparse = True)
    print "DONE with corel5k"
    
    
    ks = [197, 393, 590]
    #ks = [32]    
    Input2Output('delicious', ks, sparse = True)
    print "DONE with delicious"
    
    
    
    ks = [11, 22, 32]
    #ks = [8]
    Input2Output('enron', ks, sparse = True)
    print "DONE with enron"
    
    
    ks = [2, 3, 4]
    #ks = [3]
    Input2Output('emotions', ks, sparse = True)
    print "DONE with emotions"
    
    
    ks = [2, 3, 4]
    #ks = [3]
    Input2Output('scene', ks, sparse = True)
    print "DONE with scene"