# -*- coding: utf-8 -*-
"""
Created on Fri Feb 20 17:47:48 2015

@author: shishir
@file: train_ld.py


Train on the low dimensional output.
"""
import numpy as np
from sklearn.multiclass import OneVsRestClassifier as orc
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split as tts
import Output2Graph as otg
import heapq
import networkx as nx
#import exceptions
import pandas as pd
from sklearn.metrics import hamming_loss as hl


def TrainPredict(X_tr, Y_ld, X_tt):
    """
    Train the classifier on the data and low dimensional output.
    
    Parameters
    ---------------
    
    
    """
    rf = RandomForestClassifier()
    
    clas = orc(rf)#, y_type_.startswith('multilabel'))
    clas.fit(X_tr, Y_ld)
    
    Y_hat_ld = clas.predict_proba(X_tt)
    
    total = np.sum(Y_hat_ld, axis = 1)
    
    #low dim predicted output
    Y_hat_ld_prob = (Y_hat_ld.T/total).T
    
    return Y_hat_ld_prob


def LowDimOutput(Y, k_keys_largest):
    """
    Parameters
    -----------
    
    Y = ndarray (complete output)
    
    k_keys_largest = max page rank nodes
    
    
    Returns
    ---------------
    
    Y_ld = Low dimensional output
    
    k_keys_sorted = sorted list of the top page rank nodes
    """
    
    s_keys = sorted(k_keys_largest)
    
    Y_ld = Y[:, s_keys]
    
    return Y_ld, s_keys
    
    
def main(X, Y):
    """
    Parameters
    -----------
    
    X = input
    
    Y = output
    """
    #X, Y = otg.ReadData() 
    errors = []
    for i in xrange(20):
        print "Iterarion ", i
        X_tr, X_tt, Y_tr, Y_tt = tts(X, Y, test_size = 0.2)
        
        # get the directed output
        # even test 
        G = otg.GenerateDiGraph(Y)
        
        
        pg_rank = nx.algorithms.pagerank(G)
        #rank_dist = [pg_rank[k] for k in pg_rank.keys()]
        #print sum(rank_dist)
        
        k = 31
        if k > len(G.nodes()):
            raise ValueError("Nodes are less than the lower dimensions")
            
        k_keys_largest = heapq.nlargest(k, pg_rank, key = pg_rank.get)
        #print k_keys_largest
        
        Y_ld_tr, s_keys = LowDimOutput(Y_tr, k_keys_largest)
        
        Y_hat_ld_tt = TrainPredict(X_tr, Y_ld_tr, X_tt)
        #print Y_hat_ld_tt
        #return G, Y_hat_ld_tt, s_key
        all_ranks = PersonPageRank(G, Y_hat_ld_tt, s_keys)
        
        
        
        df = pd.DataFrame(all_ranks)
        #print df
        df.plot(kind = 'box')
        #df1 = df.copy()
        
        df[df < df.mean()] = 0
        df[df > 0] = 1
        #print df
        #print df1
        t = df.sum(axis = 1)
        if any(t == 0):
            print "XERO"
            
        Y_hat = df.to_dense()
        errors.append(hl(Y_hat, Y_tt))
        
    print np.mean(errors)
    #print hl(Y_hat, Y_tt)
    #return all_ranks
    
    
def PersonPageRank(G, Y_hat_ld_tt, s_keys):
    """
    Parameters
    ---------------
    G : networkx Graph
        It is the complete directed graph on the output nodes
        
    Y_hat_ld_tt : ndarray
        Predicted output for the lower dimensional output
        
    s_keys : sorted keys
    """
    all_ranks = []
    for row in Y_hat_ld_tt:
        if len(row) != len(s_keys):
            raise ValueError("row size and number of keys must be same")
        else :
            person_rank = {}
            ppranks = zip(s_keys, row)
            for rank in ppranks:
                person_rank[rank[0]] = rank[1]
                
            nodes = G.nodes()
            
            for node in nodes:
                person_rank.setdefault(node, 0)
                
            # run the personalized page rank algorithms
            p_page_rank = nx.algorithms.pagerank(G, \
            personalization = person_rank, max_iter = 1000)
            #print p_page_rank
            
            tmp_pg_rank = [p_page_rank[k] for k in p_page_rank]
            all_ranks.append(tmp_pg_rank)
            
    return all_ranks
            
            
            
    
if __name__ == "__main__":
    X, Y = otg.ReadData()    
    main(X, Y)
#    df = pd.DataFrame(all_ranks)
#    #print df
#    df.plot(kind = 'box')
#    df1 = df.copy()
#    
#    df[df < df.mean()] = 0
#    df[df > 0] = 1
#    print df
#    #print df1
#    t = df.sum(axis = 1)
#    if any(t == 0):
#        print "XERO"
#        
#    Y_hat = df.to_dense()
    
        
    
    
    
    
    
    