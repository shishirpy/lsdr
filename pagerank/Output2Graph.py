# -*- coding: utf-8 -*-
"""
Created on Thu Jan 29 15:02:48 2015

@author: shishir
@file: Output2Graph.py

This file will have the methods to convert output to directed graphs.
"""
from scipy import io as sio
import numpy as np
import networkx as nx
from matplotlib import pyplot as plt
import heapq
from scipy import sparse as ss


def ReadData(dataset = 'bibtex'):
    """
    Read data from data files
    """
    
    path = "../../../datasets/tsneOP/data/"
    path = path + dataset + '/'
    
    X = sio.mmread(path + 'X.mtx')
    Y = sio.mmread(path + 'y.mtx')
    
    if ss.issparse(X):
        X = X.todense()
    if ss.issparse(Y):
        Y = Y.todense()
        
    Y[Y == -1] = 0
    
    return X, Y
    
def GenerateDiGraph(Y):
    """
    This function will be used to generate the directed graph.
    
    Each node is label (columns of matrix Y). 
    """
    #Y = Y.T #now each row is a label.
    
    M = np.dot(Y.T, Y)
    M = M.astype(float)
    
    d = np.diag(M).copy()  # get the diagonal values vector
    
    for i in xrange(len(d)):
        M[i, i]= 0
        
    M = M/d
    # division by zero needs to be taken care off.
    t = np.sum(M, axis = 0)
    N = M/t
    
    if np.isnan(N).any():
        print "NAN", '\n'*2
        raise "Value Error"
    
    G = nx.DiGraph(N.T)
    #print G.edge
    return G
    
def GetTopkNodes(k, G):
    """
    calculate the largest 
    """

    
if __name__ == "__main__":
    X, Y = ReadData()
    #np.random.seed(0)
    #A = np.random.randint(0,2, (4,5))
    #print A.T
    G = GenerateDiGraph(Y)
    
    pg_rank = nx.algorithms.pagerank(G)
    rank_dist = [pg_rank[k] for k in pg_rank.keys()]
    print sum(rank_dist)
    
    k = 100
    k_keys_largest = heapq.nlargest(k, pg_rank, key = pg_rank.get)
    print k_keys_largest
    
    
    
    """
    fig  = plt.figure(figsize = (10,6))
    ax1 = fig.add_subplot(121)
    ax1.boxplot(rank_dist)
    
    ax2 = fig.add_subplot(122)
    ax2.hist(rank_dist, bins = 100)
    plt.show()
    """
    
    
    
    
    
    