# -*- coding: utf-8 -*-
"""
Created on Thu Mar 05 09:50:01 2015

@author: shishir
@file: exapnd_dim.py


This file contains the methods to exapnd the low dimensional predicted output
to the complete output
"""
#from sklearn.neighbors import NearestNeighbors
from sklearn.neighbors import KNeighborsRegressor




def NeighborsExpansion():
    """
    This method will be used to expand the low dim output to the complete
    output using the kneighborhood method.
    """