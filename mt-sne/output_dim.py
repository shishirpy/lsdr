# -*- coding: utf-8 -*-
"""
Created on Tue Mar 03 14:26:41 2015

@author: shishir
"""
import readIO
import numpy as np
from sklearn.linear_model import Ridge
from sklearn.cross_validation import train_test_split as tts
from scipy import sparse as ss
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import hamming_loss as hl


from matplotlib import pyplot as plt


datasets = ['yeast']
op_path = "outputs/"


def BinarizeOutput(Y):
    """
    Binarize the data. 
    
    Y : the output matrix is a dense matrix.
    """
    Y [Y == 0] = -1
    
    return Y

def ReadLowDimOutput(dataset, n_components = 2):
    """
    Read the low dimensional t-SNE output saved
    """
    
    file_path = 'outputs/' + dataset + '/' + dataset + '_tsne_' + \
    str(n_components) + '.txt'
    
    Y_tsne = np.loadtxt(file_path)
    return Y_tsne
    
def Train(X, Y_tsne):
    """
    Train on the low dimensional 
    """
    pass
    
def TrainPedictLowDim(dataset, n_components):
    X, Y = readIO.ReadData()
    
    Y = BinarizeOutput(Y)
    
    Y_tsne = ReadLowDimOutput(dataset = dataset, n_components = n_components)
    
    lr = Ridge(alpha = 0.1)
    for i in xrange(20):
        X_tr, X_tt, Y_tr, Y_tt, Y_tsne_tr, Y_tsne_tt = tts(X, Y, Y_tsne, \
        test_size = 0.2)
        lr.fit(X_tr, Y_tsne_tr)
        Y_hat_tsne = lr.predict(X_tt)
        
    pass




def CrossValidateforAlpha_tsne(X, Y_tsne):
    """
    This function will be used to find the alpha parameter of input to 
    Y_tsne regression.
    """
    alphas = np.logspace(-2, 3, num = 50)
    
    errors = []
    for alpha in alphas:
        lr = Ridge(alpha = alpha)
        
        # error for one alpha
        e_alpha = []
        for i in xrange(100):
            X_tr, X_tt, Y_tr, Y_tt = tts(X, Y_tsne, test_size = 0.1)
            
            lr.fit(X_tr, Y_tr)
            Y_hat = lr.predict(X_tt)
            
            e = mse(Y_hat, Y_tt)
            #print e
            e_alpha.append(e)
            
        errors.append(np.mean(e_alpha))
        #print np.mean(e_alpha)
        
    
    fig = plt.figure(figsize = (10, 6))
    ax = fig.add_subplot(111)
    ax.plot(alphas, errors)
    ax.set_xscale('log')
    
    # titles
    ax.set_xtitle('alphas')
    ax.set_ytitle('mean squared error')
    
    plt.show()
    
    
def TrainLinearPredict(Y_tsne, Y_tt):
    """
    Predict the complete output using linear regression. 
    """
    lr = Ridge(alpha  = 0.1)    
    lr.fit(Y_tsne, Y_tt)    
    return lr
    

    
    
if __name__ == "__main__":
    # read data
    dataset = 'corel5k'
    X, Y = readIO.ReadData(dataset)
    Y[Y == 0 ] = -1
    
    
    Y_tsne = ReadLowDimOutput(dataset, n_components = 5)  
    
    #CrossValidateforAlpha_tsne(X, Y_tsne)    
    #main('yeast', n_components = 2)
    
    X_tr, X_tt, Y_tr, Y_tt, Y_tsne_tr, Y_tsne_tt = tts(X, Y, Y_tsne, test_size = 0.2)
    lr2 = TrainLinearPredict(Y_tsne_tr, Y_tr)
    
    lr1 = Ridge(alpha = 10)
    lr1.fit(X_tr, Y_tsne_tr)
    
    
    Y_hat_tsne = lr1.predict(X_tt)
    Y_hat = lr2.predict(Y_hat_tsne)
    
    Y_hat = np.sign(Y_hat)
    e = hl(Y_tt, Y_hat)
    
    print e
    
    