# -*- coding: utf-8 -*-
"""
Created on Mon Mar 09 11:39:04 2015

@author: shishir
@file: rf_tsne.py
"""

import readIO
import numpy as np
#from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_validation import train_test_split as tts
from sklearn.metrics import mean_squared_error as mse, hamming_loss as hl
from sklearn.neighbors import KNeighborsRegressor




def BinarizeOutput(Y):
    """
    Binarize the data. 
    
    Y : the output matrix is a dense matrix.
    """
    Y [Y == 0] = -1
    
    return Y
    
    
def ReadLowDimOutput(dataset, n_components = 2):
    """
    Read the low dimensional t-SNE output saved
    """
    
    file_path = '../outputs/' + dataset + '/' + dataset + '_tsne_' + \
    str(n_components) + '.txt'
    
    Y_tsne = np.loadtxt(file_path)
    return Y_tsne
    
    
def mainRF():
    dataset = 'scene' #does well
    # dataset = 'bibtex'
    # dataset = 'emotions'
    # dataset = 'yeast'
    n_components = 4
    
    print "Working with dataset = ", dataset, " and n_components =", n_components
    
    X, Y = readIO.ReadData(dataset)
    Y = BinarizeOutput(Y)
    
    Y_tsne = ReadLowDimOutput(dataset = dataset, n_components = n_components)
    
    
    error = []
    for i in xrange(3):
        if i % 5 == 0:
            print "On iteration ", i
            
        X_tr, X_tt, Y_tsne_tr, Y_tsne_tt, Y_tr, Y_tt = tts(X, Y_tsne, Y, \
        test_size = 0.2)
        
        
        # input to compressed output
        rf= RandomForestRegressor(n_estimators = 20)        
        rf.fit(X_tr, Y_tsne_tr)
        Y_hat_tsne = rf.predict(X_tt)
        
        e = mse(Y_tsne_tt, Y_hat_tsne)
        
        error.append(e)
        
        
        # compressed output to full output.        
        knn = KNeighborsRegressor(n_neighbors = 20)
        knn.fit(Y_tsne_tr, Y_tr)
        Y_hat = knn.predict(Y_hat_tsne)
        Y_hat = np.sign(Y_hat+ 0.0001)
        
        print hl(Y_hat, Y_tt)
        
        
        
    print np.mean(error) 
        
        


def mainSVR():
    """
    Here we will use the Support vectro regresssions
    """
    
def mainKNN():
    """
    In this method we will be use the k-nn method in both the steps.
    """
    
#    dataset = 'scene' #does well
    dataset = 'bibtex'
    # dataset = 'emotions'
    # dataset = 'yeast'
    n_components = 3
    
    print "Working with dataset = ", dataset, " and n_components =", n_components
    
    X, Y = readIO.ReadData(dataset)
    Y = BinarizeOutput(Y)
    
    Y_tsne = ReadLowDimOutput(dataset = dataset, n_components = n_components)
    
    
    error = []
    for i in xrange(3):
        if i % 5 == 0:
            print "On iteration ", i
            
        X_tr, X_tt, Y_tsne_tr, Y_tsne_tt, Y_tr, Y_tt = tts(X, Y_tsne, Y, \
        test_size = 0.2)
        
        
        # input to compressed output
        knn1 = KNeighborsRegressor(n_neighbors = 5)
        knn1.fit(X_tr, Y_tsne_tr)
        Y_hat_tsne = knn1.predict(X_tt)
        
        e = mse(Y_tsne_tt, Y_hat_tsne)
        
        error.append(e)
        
        
        # compressed output to full output.        
        knn = KNeighborsRegressor(n_neighbors = 20)
        knn.fit(Y_tsne_tr, Y_tr)
        Y_hat = knn.predict(Y_hat_tsne)
        Y_hat = np.sign(Y_hat+ 0.0001)
        
        print hl(Y_hat, Y_tt)
        
        
        
    print np.mean(error) 
        
        
    
if __name__ == "__main__":
#    mainRF()
    mainKNN()