# -*- coding: utf-8 -*-
"""
Created on Thu Jan 29 15:02:48 2015

@author: shishir
@file: readIO.py

This file will have the methods to convert output to directed graphs.
"""
from scipy import io as sio
import numpy as np
import networkx as nx
from matplotlib import pyplot as plt
import heapq
from scipy import sparse as ss


def ReadData(dataset = 'yeast'):
    """
    Read data from data files
    """
    
    path = "../../../../datasets/tsneOP/data/"
    path = path + dataset + '/'
    
    X = sio.mmread(path + 'X.mtx')
    Y = sio.mmread(path + 'y.mtx')
    
    if ss.issparse(X):
        X = X.todense()
    if ss.issparse(Y):
        Y = Y.todense()
        
    Y[Y == -1] = 0
    
    return X, Y
    

if __name__ == "__main__":
    X, Y = ReadData()