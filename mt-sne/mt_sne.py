# -*- coding: utf-8 -*-
"""
Created on Mon Mar 02 15:51:54 2015

@author: shishir
@file : mt_sne.py
"""

#import tsne_mt
import readIO

import numpy as np
from sklearn.manifold import TSNE

#datasets = ['bibtex', 'corel5k', 'delicious', 'emotions', 'enron']
datasets = ['emotions', 'enron']

for dataset in datasets:
    X, Y = readIO.ReadData(dataset = dataset)
    print X, Y
    
    no_dims = [2, 3, 4, 5, 10, 15, 20]
    
    for n in no_dims:
        print "PRINT for n_components", n
        model = TSNE(n_components = n)
        #Y_tsne = tsne_mt.tsne_npca(Y, no_dims= n, mx2p=False)
        Y_tsne = model.fit_transform(Y)
        """
        if any(Y_tsne == np.nan):
            print "ERROR"
            print "--" * 50
        """
        path = 'outputs/' + dataset + '/'
        f_name = dataset + '_tsne_' + str(n) + '.txt'
        np.savetxt(path + f_name, Y_tsne)