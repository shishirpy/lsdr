# -*- coding: utf-8 -*-
"""
Created on Mon Mar 02 17:34:57 2015

@author: shishir
"""

from matplotlib import pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D


#####
#####
#   3D plots
# Y = np.loadtxt("outputs/yeast/yeast_tsne_3.txt")
Y = np.loadtxt("outputs/scene/scene_tsne_3.txt")
#Y = np.loadtxt("outputs/bibtex/bibtex_tsne_3.txt")
#Y = np.loadtxt("outputs/corel5k/corel5k_tsne_3.txt")
#Y = np.loadtxt("outputs/enron/enron_tsne_3.txt")
#Y = np.loadtxt("outputs/emotions/emotions_tsne_3.txt")

fig = plt.figure(figsize = (10, 6))
ax = fig.add_subplot(111, projection = '3d')
ax.scatter(Y[:, 0], Y[:, 1], Y[:, 2])
plt.show()



######
######
#   2D plots
#Y = np.loadtxt("outputs/yeast/yeast_tsne_2.txt")
Y = np.loadtxt("outputs/scene/scene_tsne_2.txt")
#Y = np.loadtxt("outputs/bibtex/bibtex_tsne_2.txt")
#Y = np.loadtxt("outputs/corel5k/corel5k_tsne_2.txt")
#Y = np.loadtxt("outputs/enron/enron_tsne_3.txt")
#Y = np.loadtxt("outputs/emotions/emotions_tsne_2.txt")

fig = plt.figure(figsize = (10, 6))
ax = fig.add_subplot(111)
ax.scatter(Y[:, 0], Y[:, 1])
plt.show()

