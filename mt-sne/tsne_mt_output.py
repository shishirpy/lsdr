# -*- coding: utf-8 -*-
"""
Created on Wed Jan 21 00:09:28 2015

@author: shishir
"""

import numpy as np
from numpy.linalg import svd
from sklearn.neighbors import NearestNeighbors as NN
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import readIO


#X = np.loadtxt('mnist2500_pca_50.txt')


def Hbeta(D = np.array([]), beta = 1.0):
    """Compute the perplexity and the P-row for a specific value of the precision of a Gaussian distribution."""
    
    # Compute P-row and corresponding perplexity
    P = np.exp(-D.copy() * beta)
    sumP = sum(P)
    H = np.log(sumP) + beta * np.sum(D * P) / sumP
    P = P / sumP
    return H, P
    

def ErrorDistances(X, n_dim = 10):
    """
    Find the error from the linear sub-space.
    
    parameters
    ----------------
    
    X : input data
    
    n_dim : default = 10
            dimension of the linear sub-space
    
    returns
    ----------------
    error from the linear sub-space
    """
    k = n_dim
    U, S, V = svd(X, full_matrices = False)    
    U = np.dot(U, np.diag(S))
    
    e = np.dot(U[:, k:], V[k:]) # error vectors
    
    errors = np.linalg.norm(e, axis = 1)
    
    return errors
    
    
def x2p(X = np.array([]), tol = 1e-5, perplexity = 30.0):
	"""Performs a binary search to get P-values in such a way that each conditional Gaussian has the same perplexity."""

	# Initialize some variables
	print "Computing pairwise distances..."
	(n, d) = X.shape
	sum_X = np.sum(np.square(X), 1)
	D = np.add(np.add(-2 * np.dot(X, X.T), sum_X).T, sum_X)
	P = np.zeros((n, n))
	beta = np.ones((n, 1))
	logU = np.log(perplexity)
    
	# Loop over all datapoints
	for i in range(n):
	
		# Print progress
		if i % 500 == 0:
			print "Computing P-values for point ", i, " of ", n, "..."
	
		# Compute the Gaussian kernel and entropy for the current precision
		betamin = -np.inf 
		betamax =  np.inf
		Di = D[i, np.concatenate((np.r_[0:i], np.r_[i+1:n]))]
		(H, thisP) = Hbeta(Di, beta[i])
			
		# Evaluate whether the perplexity is within tolerance
		Hdiff = H - logU
		tries = 0
		while np.abs(Hdiff) > tol and tries < 50:
				
			# If not, increase or decrease precision
			if Hdiff > 0:
				betamin = beta[i]
				if betamax == np.inf or betamax == -np.inf:
					beta[i] = beta[i] * 2
				else:
					beta[i] = (beta[i] + betamax) / 2
			else:
				betamax = beta[i]
				if betamin == np.inf or betamin == -np.inf:
					beta[i] = beta[i] / 2
				else:
					beta[i] = (beta[i] + betamin) / 2
			
			# Recompute the values
			(H, thisP) = Hbeta(Di, beta[i])
			Hdiff = H - logU
			tries = tries + 1
			
		# Set the final row of P
		P[i, np.concatenate((np.r_[0:i], np.r_[i+1:n]))] = thisP
	
	# Return final P-matrix
	print "Mean value of sigma: ", np.mean(np.sqrt(1 / beta))
	return P
    
    
    
def manifold_x2p(X = np.array([]), tol = 1e-5, perplexity = 30.0):
    """Performs a binary search to get P-values in such a way that each conditional Gaussian has the same perplexity."""

    # Initialize some variables
    print "Computing manifold distances..."
    (n, d) = X.shape
    sum_X = np.sum(np.square(X), 1)
    D = np.add(np.add(-2 * np.dot(X, X.T), sum_X).T, sum_X)
    
    # update the D matrix with new distances from manifold
    # only few will be updated.
    k = 50
    nn = NN(n_neighbors = k)
    nn.fit(X)
    for i in xrange(len(X)):
        nbrs = nn.kneighbors(X[i], return_distance = False)
        nbrs = nbrs.ravel()
        errors = ErrorDistances(X[nbrs], n_dim = 10)
        for idx, j in enumerate(nbrs):
            D[i, j] = errors[idx]
            
        D[i, i] = 0 # incase this has been changed
    # from here its same as old code
    
    P = np.zeros((n, n))
    beta = np.ones((n, 1))
    logU = np.log(perplexity)

    # Loop over all datapoints
    for i in range(n):
        # Print progress
        if i % 1000 == 0:
            print "Computing P-values for point ", i, " of ", n, "..."

        # Compute the Gaussian kernel and entropy for the current precision
        betamin = -np.inf
        betamax =  np.inf
        Di = D[i, np.concatenate((np.r_[0:i], np.r_[i+1:n]))]
        (H, thisP) = Hbeta(Di, beta[i])

        # Evaluate whether the perplexity is within tolerance
        Hdiff = H - logU
        tries = 0
        while np.abs(Hdiff) > tol and tries < 50:

            # If not, increase or decrease precision
            if Hdiff > 0:
                betamin = beta[i]
                if betamax == np.inf or betamax == -np.inf:
                    beta[i] = beta[i] * 2
                else:
                    beta[i] = (beta[i] + betamax) / 2
            else:
                betamax = beta[i]
                if betamin == np.inf or betamin == -np.inf:
                    beta[i] = beta[i] / 2
                else:
                    beta[i] = (beta[i] + betamin) / 2

            # Recompute the values
            (H, thisP) = Hbeta(Di, beta[i])
            Hdiff = H - logU
            tries = tries + 1

        # Set the final row of P
        P[i, np.concatenate((np.r_[0:i], np.r_[i+1:n]))] = thisP

    # Return final P-matrix
    print "Mean value of sigma: ", np.mean(np.sqrt(1 / beta))
    return P

    
    
def tsne_npca(X = np.array([]), no_dims = 2, mx2p = True, initial_dims = 50, \
perplexity = 30.0, max_iter = 500):
    """
    Runs t-SNE on the dataset in the NxD array X to reduce its dimensionality to no_dims dimensions.
    The syntaxis of the function is Y = tsne.tsne(X, no_dims, perplexity), where X is an NxD NumPy array.
    """
    # Check inputs
    if X.dtype != "float64":
        print "Error: array X should have type float64."
        return -1     
    (n, d) = X.shape
    max_iter = max_iter
    initial_momentum = 0.5
    final_momentum = 0.8
    eta = 500
    min_gain = 0.01
    Y = np.random.randn(n, no_dims)
    dY = np.zeros((n, no_dims))
    iY = np.zeros((n, no_dims))
    gains = np.ones((n, no_dims))
    
    # Compute P-values
    if mx2p:
        P = manifold_x2p(X, 1e-5, perplexity)
    else:
        P = x2p(X, 1e-5, perplexity)
    
    
    # for symmetric P
    P = P + np.transpose(P)    
    P = P / np.sum(P)
    P = P * 4									# early exaggeration
    P = np.maximum(P, 1e-12)
    
    # Run iterations
    for iter in range(max_iter):
        # Compute pairwise affinities
        sum_Y = np.sum(np.square(Y), 1)		
        num = 1 / (1 + np.add(np.add(-2 * np.dot(Y, Y.T), sum_Y).T, sum_Y))
        num[range(n), range(n)] = 0
        Q = num / np.sum(num)
        Q = np.maximum(Q, 1e-12)
        
        # Compute gradient
        PQ = P - Q
        for i in range(n):
            dY[i,:] = np.sum(np.tile(PQ[:,i] * num[:,i], (no_dims, 1)).T * (Y[i,:] - Y), 0)
            
        # Perform the update
        if iter < 20:
            momentum = initial_momentum
        else:
            momentum = final_momentum
            
        gains = (gains + 0.2) * ((dY > 0) != (iY > 0)) + (gains * 0.8) * ((dY > 0) == (iY > 0))
        gains[gains < min_gain] = min_gain
        iY = momentum * iY - eta * (gains * dY)
        Y = Y + iY
        Y = Y - np.tile(np.mean(Y, 0), (n, 1))
        
        # Compute current value of cost function
        if (iter + 1) % 10 == 0:
            C = np.sum(P * np.log(P / Q))
            print "Iteration ", (iter + 1), ": error is ", C
            
        # Stop lying about P-values
        if iter == 100:
            P = P / 4
            
    # Return solution
    return Y
    
		
  
def DigitsData():
    print "Run Y = tsne.tsne(X, no_dims, perplexity) to perform t-SNE on your dataset."
    print "Running example on 2,500 MNIST digits..."
    X = np.loadtxt("mnist2500_pca_50.txt")
    labels = np.loadtxt("mnist2500_labels.txt")
    Y = tsne_npca(X, 2, mx2p = True, initial_dims=50, perplexity=20.0)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title("t-SNE with Manifold Distances")
    ax.scatter(Y[:,0], Y[:,1], 20, labels)

    """
    Y = tsne_npca(X, 2, mx2p = False, initial_dims=50, perplexity=20.0)    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.set_title("Simpel t-SNE")
    ax1.scatter(Y[:,0], Y[:,1], 20, labels)
    """   
    plt.show()
    
    
def Renyi_DigitData():
    """
    Wrapper Method for performing the renyi sne with localization.
    """
    print "Run Y = tsne.tsne_renyi2_npca(X, alpha,  no_dims, perplexity) to perform t-SNE on your dataset."
    print "Running example on 2,500 MNIST digits..."
    X = np.loadtxt("mnist2500_pca_50.txt")
    labels = np.loadtxt("mnist2500_labels.txt")
    #X = np.loadtxt('olivetti_pca_50.csv', delimiter = ',')
    
    alphas = [0.1, 0.2, 0.5, 0.7, 0.9, 0.95, 1.05, 1.1, 1.2, \
    1.3, 1.4, 1.5, 1.6, 1.7]
    #alphas = [
    #alphas = [
    #alpha = 0.5
    for alpha in alphas:
        Y = tsne_renyi2_npca(X, alpha = alpha, mx2p=True)
        #Y = tsne(X, 2, 30, 20.0, max_iter = 200)
        
        fname = "DigitData_Renyi_" + str(alpha)        
        np.savetxt(fname, Y)
        
        print "\a"         
        fig = plt.figure(figsize = (10, 10))
        ax = fig.add_subplot(111)
        ax.scatter(Y[:,0], Y[:,1], 20, labels, alpha = 0.8)
        ax.set_title('Renyi t-SNE with alpha = ' + str(alpha))
        plt.show()
        
        
        
    
def OlivettiData():
    X = np.loadtxt('olivetti_pca_50.csv', delimiter = ',')
    
    Y = tsne_npca(X, 2, mx2p = True, initial_dims=50, perplexity=20.0)
    # since we want 40 total markers
    # we use 5 colors and 8 marker

    marker_style = ['^', 'o', '+', '*', 'x', 'd', 'v', '1']
    marker_color = ['b', 'g', 'r', 'c', 'k']
    
    t = []
    for style in marker_style:
        for color in marker_color:
            t.append((style, color))
            
    fig = plt.figure(figsize = (20, 10))
    marker = -1
    #figure(figsize = (20, 10))
    ax = fig.add_subplot(111)
    for i in xrange(Y.shape[0]):
        if i % 10 == 0:
            marker += 1
        ax.scatter(Y[i, 0], Y[i, 1], s = 80, marker = t[marker][0], c = t[marker][1], alpha = 0.8, edgecolor = t[marker][1])
    
    ax.set_title('Dimension reduction of Olivetti Faces')
    plt.show()
    
    
def COILData():
    X = np.loadtxt('COIL-20_pca_50.txt')
    labels = np.loadtxt('COIL-20_labels.txt')
    
    mx2p = True
    Y = tsne_npca(X, 3, mx2p = mx2p, initial_dims = 50, perplexity = 20.0)
    
    #np.savetxt("COIL-20_Output.txt", Y)
    np.savetxt("COIL-20_tsne_Output_1.txt", Y)
    
    
    # 2D plot
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title("t-SNE with Manifold Distances")
    ax.scatter(Y[:,0], Y[:,1], 20, labels, edgecolor = 'none', alpha = 1)
    plt.show()
    
    
    
    
    # 3D plot
    """
    np.savetxt("COIL-20-3D-output.txt", Y)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')   
    ax.scatter(Y[:,0], Y[:,1], Y[:, 2], 20, c = labels, edgecolor = 'none', alpha = 1)
    plt.show()
    """
    
    
def pca(X = np.array([]), no_dims = 50):
    """Runs PCA on the NxD array X in order to reduce its dimensionality to no_dims dimensions."""

    print "Preprocessing the data using PCA..."
    (n, d) = X.shape
    X = X - np.tile(np.mean(X, 0), (n, 1))
    (l, M) = np.linalg.eig(np.dot(X.T, X))
    Y = np.dot(X, M[:,0:no_dims])
    return Y
       
def BinarizeOutput(Y):
    """
    Binarize the data. 
    
    Y : the output matrix is a dense matrix.
    """
    Y [Y == 0] = -1
    
    return Y
    
    
def SavePCAValues():
    """
    In this we will find the pca values of the output matrices 
    and save it in a files.
    """
    datasets = ['yeast', 'enron', 'emotions', 'scene', 'bibtex', 'corel5k', 'delicious']
    
    for dataset in datasets:
        print "Working on", dataset
        print "-" * 20
        X, Y = readIO.ReadData(dataset)
        Y = BinarizeOutput(Y)
        
        m,n = Y.shape
        
        fname = dataset + '_pca_10.txt'
        if n < 10:
            np.savetxt(fname, Y)
        else:
            Y_pca = pca(Y, no_dims  = 10)
            np.savetxt(fname, Y_pca)
            
            

	
if __name__ == "__main__":
    dataset = 'yeast'
    X, Y = readIO.ReadData(dataset)
    Y = BinarizeOutput(Y)
    
    no_dims = 10 # number of 
    Y_pca = pca(Y, no_dims = no_dims)
    
    #SavePCAValues()
        
    
    Y_mtsne = tsne_npca(Y_pca, mx2p = False, perplexity=50)
    
    